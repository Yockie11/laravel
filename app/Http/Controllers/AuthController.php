<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('register');
    }

    public function welcome() {
        return view('welcome');
    }
    public function welcome_post(Request $request){
        $nama_dpn = $request['nama_dpn'];
        $nama_blkg = $request['nama_blkg'];
        $nama_lengkap = $nama_dpn . " " . $nama_blkg;
        return view('welcome', compact('nama_dpn'));
        // dd($request->all());
    }
}


