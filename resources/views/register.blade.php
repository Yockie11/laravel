<!DOCTYPE html>
<html lang="En">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook - Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <p>First Name :</p>
        <input type="text" name="nama_dpn">
        <p>Last Name :</p>
        <input type="text" name="nama_blkg">
        <br>

    <p>Gender :</p>
        <input type="radio" name="gender" value="laki-laki">Male <br>
        <input type="radio" name="gender" value="perempuan">Female <br>
        <input type="radio" name="gender" value="lainnya">Other <br>
    
    <p>Nationality :
    <br><br>
    <select name="warga_negara">
        <option value="indonesia">Indonesia</option>
        <option value="american">American</option>
        <option value="melayu">Malays</option>
        <option value="cina">Chinese</option>
        <option value="arab">Arabic</option>
        <option value="other">Other</option>
    </select>
    </p>

    <p>Language Spoken :</p>
        <input type="checkbox" name="target1" value="indonesia"> Bahasa Indonesia <br>
        <input type="checkbox" name="target2" value="inggris"> English <br>
        <input type="checkbox" name="target3" value="lainnya"> Other <br>

    <p>Bio : </p>
    <textarea name="bio" cols="30" rows="10"></textarea>

    <p><input type="submit" value="Sign Up"></p>

    </form>
</body>
</html>