<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Yockie Taupan Putra">
    <meta name="keyword" content="Berlatih HTML">
    <meta name="description" content="APA ITU HTML? HTML merupakan singkatan dari Hyper Text Markup Language. HTML adalah sebuah bahasa standar untuk pembuatan halaman web. Dengan adanya HTML, kita dapat membedakan struktur yang tersusun dari sebuah halaman melalui tag atau elemen-elemen penyusunnya.">
    <meta name="robots" content="index, follow">
    <title>SANBERCODE - Pelatihan HTML</title>
    
</head>
<body>
    <h1>SanberBook</h1>
    <h2>Social Media Developer Santai Berkualitas</h2>
    <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
    <h3>Benefit Join di SanberBook</h3>
    <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Sharing knowledge dari para mastah Sanber</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h3>Cara Bergabung ke SanberBook</h3>
   <ol type="1">
       <li>Mengunjungi Website ini</li>
       <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
       <li>Selesei!</li>
   </ol>
</body>
</html>