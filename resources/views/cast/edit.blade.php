<div>
        <h2>Edit Post {{$cast->id}}</h2>
        <form action="/casts/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->title}}" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" class="form-control" name="umur"  value="{{$cast->body}}"  id="umur" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            <div class="form-group">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" id="bio" name="bio" placeholder="Tuliskan Biografi">
                @error(bio)
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                  </div>
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>